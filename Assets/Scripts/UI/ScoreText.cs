using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ScoreText : MonoBehaviour
{
    //������� ����� ������� ��������� �������� �����, � ���� ����� ��������
    private Text text;
    private int score = 0;
    private void OnEnable()
    {
        text = GetComponent<Text>();
        EventManager.Subscribe(eEventType.AddScore, (arg) => addScore((int)arg));
        addScore(0);
    }

    private void addScore(int addedScore)
    {
        score += addedScore;
        text.text = "Score: " + score.ToString();
    }
}
