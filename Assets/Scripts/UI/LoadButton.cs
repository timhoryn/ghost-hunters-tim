using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadButton : BaseButton
{
    // ����� ������� ��������� �������� ����, � ������� ���������

    [SerializeField]
    public SceneField scene;


    public override void click()
    {
        SceneManager.LoadScene(scene);

    }
}
