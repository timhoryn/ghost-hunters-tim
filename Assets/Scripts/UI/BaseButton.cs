using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Button))]

public abstract class BaseButton : MonoBehaviour
{
    public Button button { get; private set; }

    private void OnEnable()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(delegate { click(); });
    }

    public abstract void click();
}
