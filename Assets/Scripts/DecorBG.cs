using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class DecorBG : MonoBehaviour
{
    private Camera camera;
    private void OnEnable()
    {
        camera = GetComponent<Camera>();
    }

    private void LateUpdate()
    {
        camera.backgroundColor = new Color(Mathf.PingPong(Time.time / 5f, 1), Mathf.PingPong(Time.time / 10f, 1), Mathf.PingPong(Time.time / 15f, 1));
    }
}
