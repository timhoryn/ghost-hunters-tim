using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDestroyer : MonoBehaviour
{
    private Camera camera;
    private Generator generator;

    private void OnEnable()
    {
        generator = FindObjectOfType<Generator>();
        camera = FindObjectOfType<Camera>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit.collider != null)
            {
                if (hit.transform.TryGetComponent(out Ghost ghost))
                {
                    EventManager.OnEvent(eEventType.DisableGhost, ghost.gameObject);
                    EventManager.OnEvent(eEventType.AddScore, ghost.GetScore());
                 }
            }
        }

    }
}
