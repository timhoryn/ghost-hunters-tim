using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour
{
    [SerializeField] private GameObject prefab;
    [SerializeField] private float spawnInterval = 1f;
    [SerializeField] private int maxGhosts = 10;

    private ObjectPool pool;

    private void OnEnable()
    {
        pool = new ObjectPool(prefab, transform);
        pool.SetLength(maxGhosts);

        StartCoroutine(generatorCoroutine());
        EventManager.Subscribe(eEventType.DisableGhost, (arg) => disableGhost((GameObject)arg));
    }


    private void disableGhost(GameObject obj) => pool.DisableObject(obj);

    private IEnumerator generatorCoroutine()
    {
        while (true)
        {
            Generate();

            yield return new WaitForSeconds(spawnInterval);
        }
    }

    private void Generate()
    {
        GameObject obj = pool.TryGetObject();
        if (obj)
            obj.transform.position = new Vector3(Random.Range(-2.5f, 2.5f), transform.position.y, 0);
    }


    public ObjectPool GetObjectPool()
    {
        return pool;
    }
}
