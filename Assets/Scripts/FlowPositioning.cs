using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowPositioning : MonoBehaviour
{

    private void OnEnable()
    {
        float height = FindObjectOfType<Camera>().orthographicSize;
        FindObjectOfType<Border>().transform.position = new Vector3(0, height + 1, 0);
        FindObjectOfType<Generator>().transform.position = new Vector3(0, -height - 1, 0);

    }
}
