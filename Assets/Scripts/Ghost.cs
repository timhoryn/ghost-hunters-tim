using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class Ghost : MonoBehaviour
{
    private int score = 1;
    private Rigidbody2D rigidbody2D;

    private void OnEnable()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }
    private void FixedUpdate()
    {
        rigidbody2D.MovePosition(rigidbody2D.position + (Vector2.up   * Time.deltaTime));
    }

    public int GetScore()
    {
        return score;
    }
}
