using System.Collections.Generic;
using UnityEngine;

public class ObjectPool
{
    private GameObject prefab;
    private Transform parent;
    private Stack<GameObject> objPool = new Stack<GameObject>();

    public ObjectPool(GameObject prefab, Transform parent)
    {
        this.prefab = prefab;
        this.parent = parent;
    }

    public void SetLength(int miktar)
    {
        for (int i = 0; i < miktar; i++)
        {
            GameObject obj = Object.Instantiate(prefab, parent);
            DisableObject(obj);
        }
    }

    
    public GameObject TryGetObject()
    {
        if (objPool.Count > 0)
        {
            GameObject obj = objPool.Pop();
            obj.gameObject.SetActive(true);

            return obj;
        }
        else
        {
            return null;
        }

    }

    public void DisableObject(GameObject obj)
    {
        obj.gameObject.SetActive(false);
        objPool.Push(obj);
    }
}